import "regent"

-- Helper module to handle command line arguments
local PageRankConfig = require("pagerank_config")

local c = regentlib.c

fspace Page {
  -- The rank of the page "during" the iteration. Set to 0 after every iteration
  rank_n       : double;
  -- The rank of the page at the start of the iteration
  rank_p       : double;
  -- The number of outgoing links from this page
  outlinks     : int64;
}

fspace Link (src_region : region(Page), dst_region : region(Page)){
  src : ptr(Page, src_region);
  dst : ptr(Page, dst_region);
}

terra skip_header(f : &c.FILE)
  var x : uint64, y : uint64
  c.fscanf(f, "%llu\n%llu\n", &x, &y)
end

terra read_ids(f : &c.FILE, page_ids : &uint32)
  return c.fscanf(f, "%d %d\n", &page_ids[0], &page_ids[1]) == 2
end

task initialize_graph(r_pages   : region(Page),
                      r_links   : region(Link(r_pages, r_pages)),
                      damp      : double,
                      num_pages : uint64,
                      filename  : int8[512])
where
  reads writes(r_pages, r_links)
do
  var ts_start = c.legion_get_current_time_in_micros()

  -- Also calculate the number of outgoing links for each page during the initialization phase
  for page in r_pages do
    page.rank_p = 1.0 / num_pages
    page.rank_n = 0
    page.outlinks = 0
  end

  var f = c.fopen(filename, "rb")
  skip_header(f)
  var page_ids : uint32[2]
  for link in r_links do
    regentlib.assert(read_ids(f, page_ids), "Less data that it should be")
    var src_page = dynamic_cast(ptr(Page, r_pages), page_ids[0])
    var dst_page = dynamic_cast(ptr(Page, r_pages), page_ids[1])
    
    link.src = src_page
    link.dst = dst_page
    src_page.outlinks += 1
  end
  c.fclose(f)
  var ts_stop = c.legion_get_current_time_in_micros()
  c.printf("Graph initialization took %.4f sec\n", (ts_stop - ts_start) * 1e-6)
end

task iterate_pagerank(	src_pages : region(Page),
			dst_pages : region(Page), 
			links     : region(Link(src_pages, dst_pages)))
-- Declaring permissions like reads(...), reads writes(...) throws some random assertion.
-- Also, trying to write more than one field throws an assertion.
-- Also, cannot simply use writes(dst_pages) without dereference issues
-- Also, cannot use more than one field with reduces(...)
where reads (src_pages.{rank_p, outlinks}, links.{src, dst}), reduces +(dst_pages.rank_n)
do
  var err_temp : double, l2_err : double
  l2_err = 0
  for link in links do
    link.dst.rank_n += link.src.rank_p/link.src.outlinks
  end
end

task calculate_error (pages 	: region(Page),
		      damp  	: double,
		      numPages 	: int64,
		      err	: region(double))
where reads(pages.{rank_n, rank_p}), writes(pages.{rank_n, rank_p}), reduces +(err)
do
  var sq : double
  var l2_err : double = 0
  for page in pages do
    page.rank_n = damp * page.rank_n + (1-damp)/numPages
    sq = page.rank_n - page.rank_p
    l2_err += sq*sq
    page.rank_p = page.rank_n
    page.rank_n = 0
  end
  err[0] += l2_err
end

task dump_ranks(r_pages  : region(Page),
                filename : int8[512])
where
  reads(r_pages.rank_p)
do
  var f = c.fopen(filename, "w")
  for page in r_pages do c.fprintf(f, "%g\n", page.rank_p) end
  c.fclose(f)
end

task toplevel()
  var config : PageRankConfig
  config:initialize_from_command()
  c.printf("**********************************\n")
  c.printf("* PageRank                       *\n")
  c.printf("*                                *\n")
  c.printf("* Number of Pages  : %11lu *\n",  config.num_pages)
  c.printf("* Number of Links  : %11lu *\n",  config.num_links)
  c.printf("* Damping Factor   : %11.4f *\n", config.damp)
  c.printf("* Error Bound      : %11g *\n",   config.error_bound)
  c.printf("* Max # Iterations : %11u *\n",   config.max_iterations)
  c.printf("* # Parallel Tasks : %11u *\n",   config.parallelism)
  c.printf("**********************************\n")

  -- Create a region of pages
  var r_pages = region(ispace(ptr, config.num_pages), Page)
  var r_links = region(ispace(ptr, config.num_links), Link(wild, wild))

  var err = region(ispace(ptr, 1), double)
  -- Initialize the page graph from a file
  -- Also initialize the error
  initialize_graph(r_pages, r_links, config.damp, config.num_pages, config.input)
  err[0] = 0

  -- Create the partitions.
  -- Partition the region of dst nodes equally. Calculate the edges and src nodes partitions accordingly.
  var colors = ispace(int1d, config.parallelism)

  -- partitioning 1: Independednt partitioning by destination.
  -- var dst_nodes = partition(equal, r_pages, colors) 
  -- var p_edges = preimage(r_links, dst_nodes, r_links.dst)
  -- var src_nodes = image(r_pages, p_edges, r_links.src)
  -- var error_partition = dst_nodes

  -- partitioning 2: Independent partitioning by source.
  -- var src_nodes = partition(equal, r_pages, colors)
  -- var p_edges = preimage(r_links, src_nodes, r_links.src)
  -- var dst_nodes = image(r_pages, p_edges, r_links.dst)
  -- var error_partition = src_nodes
  
  -- partitioning 3: Independent partitioning of edges.
  var p_edges = partition(equal, r_links, colors)
  var dst_nodes = image(r_pages, p_edges, r_links.dst)
  var src_nodes = image(r_pages, p_edges, r_links.src)
  var error_partition = partition(equal, r_pages, colors)
  
  var num_iterations = 0
  var converged = false
  var err_bound : double = config.error_bound * config.error_bound
  
  -- Finish all variable declarations before this point
  __fence(__execution, __block)
  var ts_start = c.legion_get_current_time_in_micros()

  __fence(__execution, __block) -- This blocks to make sure we only time the pagerank computation
  while not converged do
    num_iterations += 1
   
    __demand(__parallel) 
    for c in colors do
    	iterate_pagerank(src_nodes[c], dst_nodes[c], p_edges[c])
    end

    -- MUST use two different loops, doesn't work otherwise.
    __demand(__parallel)
    for c in colors do
	calculate_error(error_partition[c], config.damp, config.num_pages, err)
    end
    converged = (err[0] < err_bound or num_iterations == config.max_iterations)
    err[0] = 0
  end
  __fence(__execution, __block) -- This blocks to make sure we only time the pagerank computation
  var ts_stop = c.legion_get_current_time_in_micros()
  c.printf("PageRank converged after %d iterations in %.4f sec\n",
    num_iterations, (ts_stop - ts_start) * 1e-6)

  if config.dump_output then dump_ranks(r_pages, config.output) end
end

regentlib.start(toplevel)
