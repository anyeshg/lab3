import "regent"

-- Helper module to handle command line arguments
local PageRankConfig = require("pagerank_config")

local c = regentlib.c

fspace Page {
  -- The rank of the page "during" the iteration. Set to 0 after every iteration
  rank_n       : double;
  -- The rank of the page at the start of the iteration
  rank_p       : double;
  -- The number of outgoing links from this page
  outlinks     : int64;
}

fspace Link {
  src : ptr;
  dst : ptr;
}

terra skip_header(f : &c.FILE)
  var x : uint64, y : uint64
  c.fscanf(f, "%llu\n%llu\n", &x, &y)
end

terra read_ids(f : &c.FILE, page_ids : &uint32)
  return c.fscanf(f, "%d %d\n", &page_ids[0], &page_ids[1]) == 2
end

task initialize_graph(r_pages   : region(Page),
                      r_links   : region(Link),
                      damp      : double,
                      num_pages : uint64,
                      filename  : int8[512])
where
  reads writes(r_pages, r_links)
do
  var ts_start = c.legion_get_current_time_in_micros()

  -- Also calculate the number of outgoing links for each page during the initialization phase
  for page in r_pages do
    page.rank_p = 1.0 / num_pages
    page.rank_n = 0
    page.outlinks = 0
  end

  var f = c.fopen(filename, "rb")
  skip_header(f)
  var page_ids : uint32[2]
  for link in r_links do
    regentlib.assert(read_ids(f, page_ids), "Less data that it should be")
    var src_page = dynamic_cast(ptr(Page, r_pages), page_ids[0])
    var dst_page = dynamic_cast(ptr(Page, r_pages), page_ids[1])
    
    link.src = src_page
    link.dst = dst_page
    r_pages[src_page].outlinks += 1
  end
  c.fclose(f)
  var ts_stop = c.legion_get_current_time_in_micros()
  c.printf("Graph initialization took %.4f sec\n", (ts_stop - ts_start) * 1e-6)
end

task iterate_pagerank(r_pages : region(Page), r_links : region(Link), damp : double, numPages : int64)
where reads writes (r_pages), reads (r_links)
do
  var err : double, l2_err : double
  l2_err = 0
  for link in r_links do
    var src_page = link.src
    var dst_page = link.dst
    r_pages[dst_page].rank_n += r_pages[src_page].rank_p/r_pages[src_page].outlinks
  end

  for page in r_pages do
    page.rank_n = (1-damp)/numPages + damp * page.rank_n
    err = page.rank_n - page.rank_p
    l2_err += err*err
    page.rank_p = page.rank_n
    page.rank_n = 0
  end

  return l2_err
end

task dump_ranks(r_pages  : region(Page),
                filename : int8[512])
where
  reads(r_pages.rank_p)
do
  var f = c.fopen(filename, "w")
  for page in r_pages do c.fprintf(f, "%g\n", page.rank_p) end
  c.fclose(f)
end

task toplevel()
  var config : PageRankConfig
  config:initialize_from_command()
  c.printf("**********************************\n")
  c.printf("* PageRank                       *\n")
  c.printf("*                                *\n")
  c.printf("* Number of Pages  : %11lu *\n",  config.num_pages)
  c.printf("* Number of Links  : %11lu *\n",  config.num_links)
  c.printf("* Damping Factor   : %11.4f *\n", config.damp)
  c.printf("* Error Bound      : %11g *\n",   config.error_bound)
  c.printf("* Max # Iterations : %11u *\n",   config.max_iterations)
  c.printf("**********************************\n")

  -- Create a region of pages
  var r_pages = region(ispace(ptr, config.num_pages), Page)
  var r_links = region(ispace(ptr, config.num_links), Link)

  -- Initialize the page graph from a file
  initialize_graph(r_pages, r_links, config.damp, config.num_pages, config.input)

  var num_iterations = 0
  var converged = false
 
  -- Actually compare against square of the error bound to avoid calculating the square root in each iteration.
  var err : double
  var err_bound = config.error_bound * config.error_bound
  var ts_start = c.legion_get_current_time_in_micros()

  while not converged do
    num_iterations += 1
    err = iterate_pagerank(r_pages, r_links, config.damp, config.num_pages)
    converged = (err < err_bound)
  end

  var ts_stop = c.legion_get_current_time_in_micros()
  c.printf("PageRank converged after %d iterations in %.4f sec\n",
    num_iterations, (ts_stop - ts_start) * 1e-6)

  if config.dump_output then dump_ranks(r_pages, config.output) end
end

regentlib.start(toplevel)
